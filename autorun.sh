#!/usr/bin/env bash
function run {
    if ! pgrep -f $1;
    then 
        $@&
    fi
}
#run conky
#run ~/.fehbg
run lxpolkit
run nm-applet
run kdeconnect-indicator
